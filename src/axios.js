import Axios from 'axios';

const axiosInstance = Axios.create({
    // baseURL: 'http://localhost:8000/'
     baseURL: '10.1.3.5:8000/'
});

export default axiosInstance;