import React, { Component } from 'react';
// import React from 'react';
import './App.scss';
import { BrowserRouter, Route, Switch} from "react-router-dom";

//import Components
import Sidebar from './components/Sidebar/Sidebar';
import Navbar from './components/Navbar/Navbar';
import Home from './components/Home/Home';
import Clients from './components/Clients/Clients';
import Products from './components/Products/Products';
import Services from './components/Services/Services';
import Shops from './components/Shops/Shops';
import ShopAssistance from './components/ShopAssistance/ShopAssistance';
import Sellproducts from './components/ProductsforClients/ProductsforClients';
import Servicesforproducts from './components/Servicesforproducts/Servicesforproducts';
import SingleClient from './components/SingleClient/SingleClient';
import UpdateSale from './components/UpdateSale/UpdateSale';

class App extends Component {
  state = {
    response: '',
    post: '',
    responseToPost: '',
  };
  
  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ response: res.express }))
      .catch(err => console.log(err));
  }
  
  callApi = async () => {
    const response = await fetch('/new-co-app');
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    
    return body;
  };
  
  handleSubmit = async e => {
    e.preventDefault();
    const response = await fetch('/new-co-app', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ post: this.state.post }),
    });
    const body = await response.text();
    
    this.setState({ responseToPost: body });
  };
  
render() {
  return (
    <BrowserRouter>
    <div className="container-fluid">
      <Navbar />
      <div className="row">
        <div className="col-md-3"><Sidebar /></div>
        <div className="col-md-9">
        <Switch>
            <Route path={"/clients"}  exact component={(props) =>  <Clients {...props} /> }/>
            <Route path={"/clients/single"}  exact component={(props) =>  <SingleClient {...props} /> }/>
            <Route path={"/sellproducts/updateSale"}  exact component={(props) =>  <UpdateSale {...props} /> }/>
            <Route path={"/sellproducts"}  exact component={(props) =>  <Sellproducts {...props} /> }/>
            <Route path={`/products`} exact component={(props) =>  <Products {...props}/>}/>
            <Route path={`/services`} exact component={(props) =>  <Services {...props}/>}/>
            <Route path={`/shops`} exact component={(props) =>  <Shops {...props}/>}/>
            <Route path={`/servicesforproducts`} exact component={(props) =>  <Servicesforproducts {...props}/>}/>
            <Route path={`/shop-assistance`} exact component={(props) =>  <ShopAssistance {...props}/>}/>
            <Route path={"/"}  exact component={(props) =>  <Home {...props} /> }/>
            <Route>
              <h1>error 404</h1>
            </Route>
        </Switch>  
        </div>
      </div>
    </div>
    </BrowserRouter>
  );
}
}
export default App;
