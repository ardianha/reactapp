import React, {useState, useEffect} from 'react';
import axios from '../../axios';
import './Selector.scss';

const ProductSelector = (props) => {

    const [products, setProducts] = useState(null);
    const [filteredProducts, setFilteredProducts] = useState(null);
    const [activeList, setActiveList] = useState([]);
    
    useEffect(() => {
        axios({
            method: 'GET',
            url: `/products/active?id=${props.id}`
        })
            .then(res => {
                setProducts(res.data)
                setFilteredProducts(res.data)
            })
            .catch(err => console.log(err))
    }, [props])

    const searcher = (text) => {
        let searchText = text.target.value.toLowerCase();
        let filteredProducts = products.filter((product) => {
            return(product.description.toLowerCase().includes(searchText));
        })
        setFilteredProducts(filteredProducts);
    }

    const toggleClass = (index) => {
        let currentState = activeList[index];
        let resultArr = [...activeList]
        resultArr[index] = !currentState;
        setActiveList([...resultArr])
    }

    const limitText = (text) => {
        let result = "";
        for(let i = 0; i < text.length; i++){
            if(i <= 35) (result += text[i])
            else { 
                result += '...';
                break;
            }
        }
        return result;
    }

    return(
        <>
            <div className="search_area">
                <div className="active-pink-4 mb-4 __search_bar">
                    <input className="form-control" type="text" onChange={searcher} placeholder="Search Products" aria-label="Search"/>
                </div>
            </div>
            <div className="__products_selector">
                <div className="__products_list">
                    {  filteredProducts && filteredProducts.length > 0 
                        ? 
                        filteredProducts.map((el, index) => {
                            return(
                                <div key={index} className="__wrapper" id={props.doesProductsExists(el.id) ? 'active':''} onClick={() => {props.productSelection(el.id); toggleClass(index); }}>
                                    <div className="__products">
                                        <div className="row">
                                        <div className="col-12 col-md-12"><hr/></div>
                                            <div className="col-12 col-md-12">
                                                {index + 1}. {limitText(el.description)}, {el.validity}, Quantity: {el.quantity}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                       :
                       "No Product found!"
                    }
                </div>
            </div>
        </>
    )
}

export default ProductSelector;