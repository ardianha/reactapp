import React, {useState, useEffect} from 'react';
import './Shops.scss';

// libraries
import Axios from '../../axios';

const Shops = () => {
    const [showForm, setShowForm] = useState(false);
    const [shops, setShops] = useState();
    let [dataForm, setDataForm] = useState({});
    let [showEdit, setshowEdit] = useState(false);
    let [showEditButton, setshowEditButton] = useState(false);
    let [error, setError] = useState(false);

    const hideForm = () => {
        setShowForm(!showForm);
        setshowEditButton(false);
        setDataForm({});
    }

    const formName = (e) =>{
        let data = Object.assign({}, dataForm, {
            name: e.target.value
        });
        setDataForm(data);
    }

    const formCity = (e) =>{
        let data = Object.assign({}, dataForm, {
            city: e.target.value
        });
        setDataForm(data);
    }

    const editMode = () =>{
        setshowEdit(!showEdit);
        setShowForm(false);
        setDataForm({});
    }

    const deleteShop = (id) =>{
        Axios.delete(`/shops/delete`, {data : { id: id }} )
        .then(res => {
            getShops();
            setShowForm(false);
            if(res.data.name){
                setError(true);
                timer();
            }
        } )
        .catch(error => console.log(error));
    }
    
    const timer = () =>{  setTimeout(() => {
        setError(false);
      }, 5000);
    }

    const editShop = (el) => {
        let data = Object.assign({}, dataForm, {
            id: el.id,
            city: el.city,
            name: el.name
        });
        setDataForm(data);
        setShowForm(true);
        setshowEditButton(true);
    }

    const updateShop = (e) =>{
        e.preventDefault();
        Axios.put("/shops/put", dataForm)
        .then(res => {
            getShops();
         } )
        .catch(error => console.log(error));
        setShowForm(false); 
        setDataForm({});
    }

    const addShop = (e) =>{
        e.preventDefault();
        Axios.post("/shops/post", dataForm)
        .then(res => {
            getShops();
         } )
        .catch(error => console.log(error));
        setShowForm(false); 
        setDataForm({});
    }

    const getShops = () =>{
        Axios.get('/shops')
        .then((res) => setShops(res.data))
        .catch((error) => console.log(error))
    }

    useEffect(() => {
       getShops();
    }, []);

    return (
        <div className="row">
            <div className="col-lg-12 shop_page">
                <div className="row">
                    <div className="col-md-6">
                        <div className="title__box p-3">
                            <h3 className="title">Shops</h3>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="p-3">
                            {showForm 
                                ? 
                                <button onClick={() => hideForm()} className="btn btn-outline-dark">Hide Form</button>
                                : 
                                <button onClick={() => hideForm()} className="btn btn-outline-dark">Add Shop</button>
                            }  
                                {showEdit 
                                ?
                                <button  onClick={() => editMode()} className="btn btn-outline-dark ml-4">Cancel edit mode!</button>
                                : 
                                <button onClick={() => editMode()} className="btn btn-outline-dark ml-4">Edit</button>
                            }  
                        </div>
                    </div>
                </div>
                {showForm && 
                <div className="row">
                    <div className="col-md-12 form_wrapper">
                        <hr />
                        <div className="title__box p-3">
                            {showEditButton ? 
                            <h3 className="title">Edit Shop</h3>
                            :
                            <h3 className="title">Add Shop</h3>
                            }
                        </div>
                        <form>
                            <div className="form-group">
                            <label>City:</label>
                                <input type="text" onChange={formCity} value={dataForm.city || ""}  className="form-control" placeholder="City" name="description" required/>
                            </div>
                            <div className="form-group">
                            <label>Shop name:</label>
                                <input type="text"  onChange={formName} value={dataForm.name || ""} className="form-control"  placeholder="Enter Shop Name" required/>
                            </div>
                            {showEditButton ?
                            <button type="submit" onClick={updateShop} className="btn btn-outline-dark">Modify Shop</button>
                            :
                            <button type="submit" onClick={addShop} className="btn btn-outline-dark">Add Shop</button>
                            }
                        </form>
                        <hr />
                    </div>
                </div> }
                <div className="row">
                    <div className="col-lg-12">
                        {error && <p style={{color: 'red'}}>Can't delete shop!</p>}
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">City</th>
                                {showEdit && <th scope="col">Edit | Delete</th>}
                            </tr>
                            </thead>
                            <tbody>
                            {
                                shops?.map((el, index) => {
                                    return (
                                        <tr key={index}>
                                            <td>{el.name}</td>
                                            <td>{el.city}</td>
                                            {showEdit && <td><span className="edit_button" onClick={() => editShop(el)}>Edit</span> | <span  onClick={() => deleteShop(el.id)} className="delete_button">Delete</span></td>}
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Shops;