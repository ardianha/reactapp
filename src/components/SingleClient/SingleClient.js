import React, {useState, useEffect} from 'react';
import './SingleClient.scss';
import queryString from 'query-string';

// libraries
import Axios from '../../axios';

const Clients = (props) => {
    const [ client, setClient ] = useState([]);
    const [ clientData, setClientData ] = useState([]);
    const [ msgData, setMsgData ] = useState(false);
 

    useEffect(() => {
        const id = queryString.parse(props.location.search).id;

        Axios.get('/clients_products_rel/one',{params: {id: id}})
        .then((res) => {
            setClient(res.data)
            let data = res.data.length;
            if(Number(data) === 0){
                setMsgData(true)
            }
        })
        .catch((error) => console.log(error))
        let userData = props.history.location.state;
        setClientData(userData);
       
    },[props]);


    return (
        <div className="row">
            <div className="col-lg-12 client_wrapper">
                <div className="row">
                    <div className="col-md-6 p-4">
                        <div className="title__box">
                            <p className="title">Client: <b>{ clientData?.name } { clientData?.surname }</b> </p>
                            <p className="title">Address: <b>{ clientData?.address }</b> </p>
                            <p className="title">Phone number: <b>{ clientData?.phone_number }</b>  </p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Description</th>
                                    <th scope="col">Validity</th>
                                    <th scope="col">Quantity</th>
                                    <th scope="col">Shop name</th>
                                    <th scope="col">City</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    client?.map((el, index) => {
                                        return (
                                                <tr  key={index}>
                                                    <td>{el.product.description}</td>
                                                    <td>{el.product.validity}</td>
                                                    <td>{el.product.quantity}</td>
                                                    <td>{el.product.shop.name}</td>
                                                    <td>{el.product.shop.city}</td>
                                                </tr>
                                            )
                                        }
                                        )
                                }
                                </tbody>
                            </table>
                            {msgData &&
                                <p>No data!</p>
                            }                               
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Clients;