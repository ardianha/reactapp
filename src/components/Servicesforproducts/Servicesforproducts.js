import React, { useState, useEffect } from 'react';
import './Servicesforproducts.scss';

// libraries
import Axios from '../../axios';

const Servicesforproducts = () => {
    const [services, setServices] = useState();
    const [products, setProducts] = useState();
    const [all_products_services_rel, set_products_services_rel] = useState();
    const [editMode, setEditMode] = useState(false);
    const [editModeButton, setEditModeButton] = useState(false);
    const [showForm, setShowForm] = useState(false);
    let [dataForm, setDataForm] = useState({});

    const hideForm = () => {
        setShowForm(!showForm);
        setDataForm({});
        if(showForm){
            setEditModeButton(false);
            setDataForm({});
        }
    } 

    const editButton = () =>{
        setEditMode(!editMode);
        setEditModeButton(false);
        if(showForm){
            setShowForm(false);
        }
    }

    const formService = (e) =>{
        let data = Object.assign({}, dataForm, {
            service_id: e
        });
        setDataForm( data );
        console.log(data) ;
    }
    
    const formProduct = (e) =>{
        let data = Object.assign({}, dataForm, {
            product_id: e
        });
        setDataForm(data);
        // console.log(data);
    }

    const updateRelation = (e) => {
        e.preventDefault();
        Axios.put("/products_services_rel/put", dataForm)
        .then(res => {
           getAll();
         } )
        .catch(error => console.log(error));
        setShowForm(false); 
    }

    const addRelation = (e) => {
        e.preventDefault();
        Axios.post("/products_services_rel/post", dataForm)
        .then(res =>{
            setShowForm(false);
            setDataForm({});
            getAll();
        } )
        .catch(error => console.log(error));
    }

    const deleteRelation = (id) => {
         Axios.delete(`/products_services_rel/delete`, {data : { id: id }} )
        .then(res => {
            getAll();
            if(res.data.name){
                getAll();
            }
        } )
        .catch(error => console.log(error));
    }

    const edit = (el) => {
        let data = Object.assign({}, dataForm, {
            id: el.id,
            product_id: el.product_id,
            service_id: el.service_id
        });
        setDataForm(data);
        setShowForm(true);
        setEditModeButton(true);
    }

    const getProducts = () =>{
        Axios.get('/products')
        .then((res) => setProducts(res.data))
        .catch((error) => console.log(error))
    }

    // console.log(services);
    const getServices = () =>{
        Axios.get('/services')
        .then((res) => setServices(res.data))
        .catch((error) => console.log(error))
    }

    const getAll = () => {
        Axios.get('/products_services_rel')
        .then((res) => set_products_services_rel(res.data))
        .catch((error) => console.log(error))
    }

    useEffect(() => {
      getServices();
      getProducts();
      getAll();
    }, []);

    return (
        <div className="row">
            <div className="col-lg-12 client_wrapper">
                <div className="row">
                    <div className="col-md-6 p-4">
                        <div className="title__box">
                            <h3 className="title">Services to Products</h3>
                        </div>
                    </div>
                    <div className="col-md-6 p-4">
                       
                        {showForm 
                            ?  
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Hide Form Add</button>
                            : 
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Add services to Products</button>
                        }  
                          {editMode 
                            ?  
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Hide edit mode</button>
                            : 
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Edit</button>
                        }  
                       
                    </div>
                </div>
                {showForm && 
                <div className="row">
                    <div className="col-md-12">
                        <hr />
                        <form>
                            <div className="form-group">
                            <p>Select Product:</p>
                                <ul id="products">
                                    {products?.map((el, index) => {
                                        return(
                                        <li  className={(dataForm.product_id === el.id) ? "active" : "" }  onClick={()=>formProduct(el.id)}  value={el.id} key={index}>{el.description}</li>
                                        )
                                    })}
                                </ul>
                            </div>
                            <hr />
                            <div className="form-group">
                            <p>Select Service:</p>
                                <ul id="products">
                                    {services?.map((el, index) => {
                                        return(
                                        <li className={(dataForm.service_id === el.id) ? "active" : "" } onClick={() => formService(el.id)} key={index}>{el.description}</li>
                                        )
                                    })}
                                </ul>
                           </div>
                            
                            {editModeButton ?
                                <button onClick={updateRelation} className="btn btn-outline-dark">Update Service to Product</button>
                                :
                                <button onClick={addRelation} className="btn btn-outline-dark">Add Service to Product</button>
                                }
                        </form>
                        <hr />
                    </div>
                </div> }
                <div className="row">
                    <div className="col-lg-12">
                        <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Service description</th>
                                    <th scope="col">Service Price</th>
                                    <th scope="col">Product description</th>
                                    <th scope="col">Product validity</th>
                                    {editMode && <th scope="col">Edit | Delete</th>}
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    all_products_services_rel?.map((el, index) => {
                                        return (
                                            <tr  key={index}>
                                                <td>{el.service.description}</td>
                                                <td>{el.service.price} $</td>
                                                <td>{el.product.description}</td>
                                                <td>{el.product.validity}</td>
                                                {editMode && <td>
                                                <span className="edit_button" onClick={() => edit(el)}> Edit </span> 
                                                | 
                                                <span  onClick={() => deleteRelation(el.id)} className="delete_button"> Delete </span>
                                                </td>}
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Servicesforproducts;