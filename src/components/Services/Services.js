import React, {useState, useEffect} from 'react';
import './Services.scss';

// libraries
import Axios from 'axios';

const Services = () => {

    const [services, setServices] = useState();
    const [showForm, setShowForm] = useState(false);
    const [editMode, setEditMode] = useState(false);
    const [editModeButton, setEditModeButton] = useState(false);
    let [dataForm, setDataForm] = useState({});


    const hideForm = () => {
        setShowForm(!showForm);
        setDataForm({});
    }

    const editButton = () => {
        setEditMode(!editMode);
        setShowForm(false);
        setDataForm({});
    }

    const formDescription = (e) =>{
        let data = Object.assign({}, dataForm, {
            description: e.target.value
        });
        setDataForm(data);
    }

    const formState = (e) =>{
        let data = Object.assign({}, dataForm, {
            status: e.target.value
        });
        setDataForm( data );
    }

    const formPrice = (e) =>{
        let data = Object.assign({}, dataForm, {
            price: e.target.value
        });
        setDataForm( data );
    }

  


    const editService = (el) =>{
        let data = Object.assign({}, dataForm, {
            id: el.id,
            description: el.description,
            status: el.status,
            price: el.price,
        });
        setDataForm(data);
        setShowForm(true);
        setEditModeButton(true);
    }

    const deleteService = (el) =>{
        Axios.delete(`http://localhost:8000/services/delete`, {data : { id: el }} )
        .then(res => {
            getAllServices();
            if(res.data.name){
            }
        } )
        .catch(error => console.log(error));
    }

    const updateService = (e) => {
        e.preventDefault();
        Axios.put("http://localhost:8000/services/put", dataForm)
        .then(res => {
            getAllServices();
         } )
        .catch(error => console.log(error));
        setShowForm(false); 
    }

    const addService = (e) => {
        e.preventDefault();
        Axios.post("http://localhost:8000/services/post", dataForm)
        .then(res =>{
            setShowForm(false);
            setDataForm({});
            getAllServices();
        } )
        .catch(error => console.log(error));
    }


    const getAllServices = () => {
        Axios.get('http://localhost:8000/services')
        .then((res) => setServices(res.data))
        .catch((error) => console.log(error))
    }

    useEffect(() => {
        getAllServices();

    }, []);

    return (
        <div className="row">
            <div className="col-lg-12 services_wrapper">
            <div className="row">
                    <div className="col-lg-6 p-4">
                        <div className="title__box">
                            <h3 className="title">SERVICES</h3>
                        </div>
                    </div>
                    <div className="col-lg-6 p-4">
                        {showForm 
                            ?  
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Hide Form Add</button>
                            : 
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Add Service</button>
                        }  
                          {editMode 
                            ?  
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Hide edit mode</button>
                            : 
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Edit</button>
                        }  
                    </div>
                </div>
                {showForm && 
                <div className="row">
                    <div className="col-md-12">
                        <hr />
                        <div className="title__box p-3">
                            {editModeButton ?
                                <h3 className="title">Edit Service</h3>
                                :
                                <h3 className="title">Add Service</h3>
                            }
                        </div>
                        <form>
                            <div className="form-group">
                            <label>Description:</label>
                                <input type="text" onChange={formDescription}  value={dataForm.description || ""} className="form-control" placeholder="Description" name="description" required/>
                            </div>
                            <div className="form-group">
                            <label>Price:</label>
                                <input type="number"  onChange={formPrice}  value={dataForm.price || ""} className="form-control"  placeholder="Price" name="date" required/>
                            </div>
                            <div className="form-group">
                            <p>Status:</p>
                            <label>
                                <input type="radio"  onChange={formState} id="acitve" checked={(Number(dataForm.status) === 1) ? true : false } name="state" value="1" />
                                Acitve</label><br />
                                <label>
                                <input type="radio"  onChange={formState} id="disable" checked={(Number(dataForm.status) === 0) ? true : false }  name="state" value="0" />
                                Disable</label>
                            </div>
                            {editModeButton ?
                                <button onClick={updateService} className="btn btn-outline-dark">Update Service</button>
                                :
                                <button onClick={addService} className="btn btn-outline-dark">Add Service</button>
                                }
                        </form>
                        <hr />
                    </div>
                </div> }
                <div className="row">
                    <div className="col-lg-12">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Description</th>
                                <th scope="col">Price</th>
                                <th scope="col">Status</th>
                                {editMode && <th scope="col">Edit | Delete</th>}
                            </tr>
                            </thead>
                            <tbody>
                            {
                                services?.map((el, index) => {
                                    return (
                                        <tr  key={index}>
                                            <td>{el.description}</td>
                                            <td>{el.price}</td>
                                            <td>{el.status ? <span className="active_state"> Active</span> : <span className="disable_state"> Disable </span>}</td>
                                            {editMode && <td>
                                                <span className="edit_button" onClick={() => editService(el)}> Edit </span> 
                                                | 
                                                <span  onClick={() => deleteService(el.id)} className="delete_button"> Delete </span>
                                                </td>}
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Services;