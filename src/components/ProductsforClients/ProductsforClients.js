import React, { useState, useEffect } from 'react';
import './ProductsforClients.scss';

// libraries
import Axios from '../../axios';

//Components
import ProductSelector from '../Selector/Selector';

const ProductsforClients = (props) => {
    const [clients, setClients] = useState();
    const [filterClients , setFilterClients] = useState();
    const [all_clients_products_rel, set_clients_products_rel] = useState();
    const [hidden, setHidden] = useState(false);
    const [shops, setShops] = useState([]);

    const [editMode, setEditMode] = useState(false);
    const [editModeButton, setEditModeButton] = useState(false);
    const [showForm, setShowForm] = useState(false);
    let [dataForm, setDataForm] = useState({});
    let [ shopAssistants, setShopAssistants] = useState([]);

    const [msg, setMsg] = useState(null);
    let [ selectProducts, setProducts] = useState([]);
    let [ clientId, setClientId] = useState(null);
    let [ shopId, setShopId] = useState(null);
    let [ shopAssistantsId, setShopAssistantsId] = useState(null);

    const hideForm = () => {
        setShowForm(!showForm);
        setDataForm({});
        if(showForm){
            setEditModeButton(false);
            setDataForm({});
        }
    } 

    const sellProducts = () => {
        Axios.get('/shops')
        .then((res) => {
            setShops(res.data)
            // console.log(res.data)
        })
        .catch((error) => console.log(error))
        setShowForm(!showForm);
        setDataForm({});
        if(showForm){
            setEditModeButton(false);
            setDataForm({});
        }
    } 

    const editButton = () =>{
        setEditMode(!editMode);
        setEditModeButton(false);
        if(showForm){
            setShowForm(false);
        }
    }

    const selectClient = (id) =>{
        let data = Object.assign({}, dataForm, {
            client_id: id
        });
        setDataForm( data );
        setClientId(id);
        setHidden(!hidden);
    }

    const selectShopAssistants = (id) =>{
        let data = Object.assign({}, dataForm, {
            shop_assistants_id	: id
        });
        setDataForm( data );
        setShopAssistantsId(id);
    }

    const selectShop = (id) =>{
        Axios.get(`/shop_assistants/onlyselected?id=${id}`)
        .then((res) => {
            setShopAssistants(res.data)
        })
        let data = Object.assign({}, dataForm, {
            shop_id: id
        });
        setDataForm( data );
        setShopId(id);
    }
    
    const updateRelation = (e) => {
        e.preventDefault();
        Axios.put("/clients_products_rel/put", dataForm)
        .then(res => {
           getAll();
         } )
        .catch(error => console.log(error));
        setShowForm(false); 
    }

    const deleteRelation = (id) => {
         Axios.delete(`/clients_products_rel/delete`, {data : { id: id }} )
        .then(res => {
            getAll();
            if(res.data.name){
                getAll();
            }
        } )
        .catch(error => console.log(error));
    }

    const editClient = (el) => {

        props.history.push({
            pathname: `/sellproducts/updateSale`,
            search: `?id=${el.id}`,
            state: {
                id: el.id,
                clientID: el.client.id,
                productId: el.product.id,
                name: el.client.name,
                surname: el.client.surname, 
                address: el.client.address, 
                phone_number: el.client.phone_number
            }
        });
    }

    //  
    const doesProductsExists = (id) => {
        return selectProducts.find(el => {return el === id});
    }

    const productSelection = (id) => {

        let exists = selectProducts.find(el => {return el === id});        
        if(!exists){
            setProducts([...selectProducts, id])
        }
        else{
            setProducts(selectProducts.filter(el => (el !== id)))
        }
    }

    const addRelation = (e) => {
        e.preventDefault();
        Axios({
            method: 'POST',
            url: '/clients_products_rel/assignproducts',
            data: { products: selectProducts, clientId: clientId , shopAssistantsId: shopAssistantsId}
        })
            .then(res => {
                setMsg(res.data.msg);
                setTimeout(() => {
                        setShowForm(false);
                        setDataForm({});
                        getAll();
                        setProducts([]);
                        setClientId("")
                        setMsg("");
                        setHidden(false);
                }, 1500)
            })
            .catch(err => {
                console.log(err);
            })
    }

    const searcherClient = (text) => {
        let searchText = text.target.value.toLowerCase();
        let filteredClient = clients.filter((client) => {
            return(client.name.toLowerCase().includes(searchText) ||
                client.surname.toLowerCase().includes(searchText));
        })
        setFilterClients(filteredClient)
    }




    // console.log(services);
    const getClients = () =>{
        Axios.get('/clients/all')
        .then((res) => {
            setClients(res.data)
            setFilterClients(res.data)
        })
        .catch((error) => console.log(error))
    }

    const getAll = () => {
        Axios.get('/clients_products_rel')
        .then((res) => set_clients_products_rel(res.data))
        .catch((error) => console.log(error))
    }

    useEffect(() => {
      getClients();
      getAll();
    }, []);

    return (
        <div className="row">
            <div className="col-lg-12 client_wrapper">
                <div className="row">
                    <div className="col-md-6 p-4">
                        <div className="title__box">
                            <h3 className="title">Products on sale</h3>
                        </div>
                    </div>
                    <div className="col-md-6 p-4">
                       
                        {showForm 
                            ?  
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Hide Form Add</button>
                            : 
                            <button onClick={() => sellProducts()} className="btn btn-outline-dark">Sell Product</button>
                        }  
                          {editMode 
                            ?  
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Hide edit mode</button>
                            : 
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Edit</button>
                        }  
                       
                    </div>
                </div>
                {showForm && 
                <div className="row">
                    <div className="col-md-12">
                        <form>
                        <div className="search_area">
                            <div className="form-group">
                                    <div className="active-pink-4 search_bar">
                                      <span className="pr-5">Select Client: </span>  <input className="form-contro" type="text" onChange={searcherClient} placeholder="Search Client" aria-label="Search"/>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group">
                                <hr/>
                                <ul>
                                    {filterClients?.map((el, index) => {
                                        return(
                                        <li title={hidden ? "Click to see all Clients!" : "Select one!"} className={(Number(el.id) === Number(clientId)) ? "active" : `list_style ${hidden}`} key={index} onClick={()=> selectClient(el.id)}>
                                            {el.name} {el.surname} {el.phone_number}
                                        </li>
                                        )
                                    })}
                               </ul>
                           </div>
                           <div className="form-group">
                                <div className="row">
                                    <div className="col-md-6">
                                    <p>Select Shop:</p>
                                    <ul>
                                        {shops?.map((el, index) => {
                                            return(
                                            <li className={(Number(el.id) === Number(shopId)) ? "active" : `list_style`} key={index} onClick={()=> selectShop(el.id)}>
                                                 {el.name} - {el.city}
                                            </li>
                                            )
                                        })}
                                    </ul>
                                    </div>
                                    <div className="col-md-6">
                                    <p>Select Assistants:</p>
                                    <ul>
                                        {shopAssistants?.map((el, index) => {
                                            return(
                                            <li className={(Number(el.id) === Number(dataForm.shop_assistants_id)) ? "active" : `list_style`} key={index} onClick={()=> selectShopAssistants(el.id)}>
                                                 {el.name}
                                            </li>
                                            )
                                        })}
                                    </ul>
                                    </div>
                                </div>
                           </div>
                            <div className="form-group">
                            <p>Select Product:</p>
                                <hr/>
                                <ProductSelector productSelection = {productSelection} doesProductsExists = {doesProductsExists} id={shopId} />
                            </div>
                            <hr />
                            {editModeButton ?
                                <button onClick={updateRelation} className="btn btn-outline-dark">Update</button>
                                :
                                <button onClick={addRelation} className="btn btn-outline-dark">Sell Product</button>
                                }
                        </form>
                            {msg && <p className="p-3" style={{color: "green"}}><b>{msg}</b></p>}
                        <hr />
                    </div>
                </div> }
                <div className="row">
                    <div className="col-lg-12">
                        <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Client</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Validity</th>
                                    <th scope="col">Shop</th>
                                    {editMode && <th scope="col">Edit | Delete</th>}
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    all_clients_products_rel?.map((el, index) => {
                                        return (
                                            <tr  key={index}>
                                                <td>{el.client?.name} {el.client?.surname}</td>
                                                <td>{el.product.description}</td>
                                                <td>{el.product.validity}</td>
                                                <td>{el?.product.shop?.name} - {el?.shop_assistant?.name}</td>
                                                {editMode && <td>
                                                <span className="edit_button" onClick={() => editClient(el)}> Edit </span> 
                                                | 
                                                <span  onClick={() => deleteRelation(el.id)} className="delete_button"> Delete </span>
                                                </td>}
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ProductsforClients;