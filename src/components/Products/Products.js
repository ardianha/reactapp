import React, { useState, useEffect } from 'react';
import './Products.scss';

// libraries
import Axios from '../../axios';

function Products(){
    const [products, setProducts] = useState();
    const [editMode, setEditMode] = useState(false);
    const [editModeButton, setEditModeButton] = useState(false);
    const [shops, setShops] = useState();
    const [showForm, setShowForm] = useState(false);
    let [dataForm, setDataForm] = useState({});

    const addproducts = (e) => {
        e.preventDefault();
        Axios.post("/products/post", dataForm)
        .then(res =>{
            setShowForm(false);
            setDataForm({});
            getProductsAll();
        } )
        .catch(error => console.log(error));
    }

    const formName = (e) =>{
        let data = Object.assign({}, dataForm, {
            description: e.target.value
        });
        setDataForm(data);
    }

    const formState = (e) =>{
        let data = Object.assign({}, dataForm, {
            state: e.target.value
        });
        setDataForm( data );
    }
    const formValidity = (e) =>{
        let data = Object.assign({}, dataForm, {
            validity: e.target.value
        });
        setDataForm( data );
    }

    const formShop = (e) =>{
        let data = Object.assign({}, dataForm, {
            shop_id: e.target.value
        });
        setDataForm( data );
    }

    const formQuantity = (e) =>{
        let data = Object.assign({}, dataForm, {
            quantity: e.target.value
        });
        setDataForm( data ); 
    }

    const hideForm = () => {
        setShowForm(!showForm);
        setDataForm({});
        if(showForm){
            setEditModeButton(false);
            setDataForm({});
        }
    } 

    const editButton = () =>{
        setEditMode(!editMode);
        setEditModeButton(false);
        if(showForm){
            setShowForm(false);
        }
    }

    const deleteProduct = (id) =>{
        // console.log(id)
        Axios.delete(`/products/delete`, {data : { id: id }} )
        .then(res => {
            console.log(res.data)
            getProductsAll();
            if(res.data.name){
            }
        } )
        .catch(error => console.log(error));
    }

    const editProduct = (el) =>{
        let data = Object.assign({}, dataForm, {
            id: el.id,
            description: el.description,
            validity: el.validity,
            state: el.state,
            shop_id: el.shop_id,
            quantity: el.quantity,
        });
        setDataForm(data);
        setShowForm(true);
        setEditModeButton(true);
    }

    const updateProduct = (e) =>{
        e.preventDefault();
        Axios.put("/products/put", dataForm)
        .then(res => {
            getProductsAll();
         } )
        .catch(error => console.log(error));
        setShowForm(false); 
    }

    const getProductsAll = () =>{
        Axios.get('/products')
        .then((res) => setProducts(res.data))
        .catch((error) => console.log(error));
    }

    useEffect(() => {
        getProductsAll();
        Axios.get('/shops')
            .then((res) => setShops(res.data))
            .catch((error) => console.log(error))
    }, []);

    return (
        <div className="row">
            <div className="col-lg-12 product_wrapper">
                <div className="row">
                    <div className="col-md-6">
                        <div className="title__box p-3">
                            <h3 className="title">Products</h3>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="p-3">
                        {showForm 
                            ?  
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Hide Form Add</button>
                            : 
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Add Products</button>
                        }  
                          {editMode 
                            ?  
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Hide edit mode</button>
                            : 
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Edit</button>
                        }  
                        </div>
                    </div>
                </div>
                {showForm && 
                <div className="row">
                    <div className="col-md-12">
                        <hr />
                        <div className="title__box p-3">
                            {editModeButton ?
                                <h3 className="title">Edit Product</h3>
                                :
                                <h3 className="title">Add Product</h3>
                            }
                        </div>
                        <form>
                            <div className="form-group">
                            <label>Description:</label>
                                <input type="text" onChange={formName}  value={dataForm.description || ""} className="form-control" placeholder="Description" name="description" required/>
                            </div>
                            <div className="form-group">
                            <label>Validity:</label>
                                <input type="date"  onChange={formValidity}  value={dataForm.validity || ""} className="form-control"  placeholder="Enter Date" name="date" required/>
                            </div>
                            <div className="form-group">
                            <p>State:</p>
                            <label name="state" >
                                <input type="radio"  onChange={formState}  checked={(Number(dataForm.state) === 1 ) ?  true :  false } id="acitve" name="state" value="1" />
                                Acitve</label><br />
                                <label>
                                <input type="radio"  onChange={formState}    checked={(Number(dataForm.state) === 0 ) ? true :  false}  id="disable" name="state" value="0" />
                                Disable</label>
                            </div>
                            <div className="form-group">
                            <label>Select shop: </label>
                                <select id="cars"  value={dataForm.shop_id || ""} onChange={formShop}>
                                    <option value=""> Select Shop </option>
                                    { shops?.map((el, index)=>{
                                        return(
                                            <option value={el.id} key={index}> {el.name} </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <div className="form-group">
                            <label>Quantity:</label>
                                <input type="number"  onChange={formQuantity}  value={dataForm.quantity || ""} className="form-control"  placeholder="Quantity" name="date" required/>
                            </div>
                            
                            {editModeButton ?
                                <button onClick={updateProduct} className="btn btn-outline-dark">Update Products</button>
                                :
                                <button onClick={addproducts} className="btn btn-outline-dark">Add Products</button>
                                }
                        </form>
                        <hr />
                    </div>
                </div> }
                <div className="row">
                    <div className="col-lg-12">
                        <table className="table table-hover">
                            <thead>
                            <tr>
                                <th scope="col">Description</th>
                                <th scope="col">Validity</th>
                                <th scope="col">State</th>
                                <th scope="col">Shop</th>
                                <th scope="col">Quantity</th>
                                {editMode && <th scope="col">Edit | Delete</th>}
                            </tr>
                            </thead>
                            <tbody>
                            {
                                products?.map((el, index) => {
                                    return (
                                        <tr  key={index}>
                                            <td>{el.description}</td>
                                            <td>{el.validity}</td>
                                            <td>{el.state ? <span className="active_state"> Active</span> : <span className="disable_state"> Disable </span>}</td>
                                            <td>{el.shop.name}</td>
                                            <td>{el.quantity}</td>
                                            {editMode && <td>
                                                <span className="edit_button" onClick={() => editProduct(el)}> Edit </span> 
                                                | 
                                                <span  onClick={() => deleteProduct(el.id)} className="delete_button"> Delete </span>
                                                </td>}
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Products;