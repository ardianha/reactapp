import React, {useState, useEffect} from 'react';
import './Home.scss';

// libraries
import Axios from '../../axios';

const Home = () => {
    const [ dataShopAssistants, seDdataShopAssistants ] = useState();
    const [ sellservices ] = useState();
    // const [ sellservices, setSellservices ] = useState();

    useEffect(()=>{
        // Axios.get('/procedure/sellservices')
        // // .then((res) => {setSellservices(res.data)})
        // .then((res) => {console.log(res.data)})
        // .catch((error) => console.log(error))
        Axios.get('/procedure/shopassistants')
        .then((res) => {seDdataShopAssistants(res.data)})
        .catch((error) => console.log(error))
    },[])
    
    return (
        <div className="container">
            <div className="row">
                <div className="col-lg-12 home-wrapper">
                    <div className="wrapper">
                        <h3>Wellcome</h3>
                        <h2 className="text-center">
                            <span className="logo__">CO</span>
                            NewCo
                        </h2>
                    </div>
                </div>
                <div className="col-lg-12 home-wrapper">
                <div className='row'>
                    {sellservices?.map((el, index)=>{
                        return(
                        <div className="col-md-12 view_procedure " key={index}>
                            {<h6>Best services</h6>}
                            <p><b>{el.serviceDescription}</b> - Product name: <b>{el.productDescription}</b></p>
                            <p>Shop: {el.shopName}</p>
                        </div>
                        )
                    })}
                    </div> 
                    <hr/>
                    <div className='row'>
                    {dataShopAssistants?.map((el, index)=>{
                        return(
                        <div className="col-md-6 view_procedure " key={index}>
                            {(index === 0) ? <h6>Best Seller</h6> :  <h6>Worst Seller</h6>}
                            <h5>Name: {el.name}</h5>
                            <p>Shopname: {el.shopname}</p>
                            <p>Sales Product: {el.saledProducts}</p>
                        </div>
                        )
                    })}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;