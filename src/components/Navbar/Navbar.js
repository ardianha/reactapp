import React from 'react';
import './Navbar.scss';

const Navbar = (props) => {
    return (
        <div className="row">
            <div className="col-lg-12 Navbar">
                <h2 className="text-center">NewCo</h2>
            </div>
        </div>
    );
};

export default Navbar;