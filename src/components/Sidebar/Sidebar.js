import React from 'react';
import './Sidebar.scss';

import { NavLink } from 'react-router-dom';

const Sidebar = () => {
    return (
        <div className="Sidebar">
            <div className="title__box">
                <NavLink to="/">
                    <span className="logo__">
                        CO
                    </span>
                    <span className="text">
                        NewCo
                    </span>
                </NavLink>
            </div>
            <ul>
                <li><NavLink activeClassName="active" to="/clients"><i className="fab fa-airbnb"></i>Clients</NavLink></li>
                <li><NavLink activeClassName="active" to="/sellproducts"><i className="fab fa-airbnb"></i>On Sale </NavLink></li>
                <li><NavLink activeClassName="active" to="/products"><i className="fab fa-airbnb"></i>Products</NavLink></li>
                <li><NavLink activeClassName="active" to="/services"><i className="fab fa-airbnb"></i>Services</NavLink></li>
                <li><NavLink activeClassName="active" to="/servicesforproducts"><i className="fab fa-airbnb"></i>Products services</NavLink></li>
                <li><NavLink activeClassName="active" to="/shops"><i className="fab fa-airbnb"></i>Shops</NavLink></li>
                <li><NavLink activeClassName="active" to="/shop-assistance"><i className="fab fa-airbnb"></i>Shop Assistance</NavLink></li>
            </ul>
        </div>
    );
};

export default Sidebar;