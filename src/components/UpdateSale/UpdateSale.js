import React, {useState, useEffect} from 'react';
import Axios from '../../axios';
import './UpdateSale.scss';

const UpdateSale = (props) => {
    const [ products, setProducts] = useState();
    const [ filterProducts , setFilteredProducts] = useState();
    const [ clientInfo , setClientsInfo] = useState();
    let [ data, setDataForm] = useState({});
    const [msg, setMsg] = useState(null);
    let [ clientId, setClientId] = useState(null);
    let [ saveProductId, setSaveProductId] = useState(null);
    let [ shopId, setShopId] = useState(null);
    const [shops, setShops] = useState();
    const [shopAssistants, setShopAssistants] = useState();

    const selectClient = (id) =>{
        let data1 = Object.assign({}, data, {
            product_id: id,
            saveProductId: saveProductId
        });
        setDataForm( data1 );
        setClientId(id);
    }

    const searcherProducts = (text) => {
        let searchText = text.target.value.toLowerCase();
        let filteredClient = products.filter((products) => {
            return(products.description.toLowerCase().includes(searchText));
        })
        setFilteredProducts(filteredClient)
    }

    const updateRelation = (e) => {
        e.preventDefault();
        Axios.put("/clients_products_rel/put", data)
        .then(res => {
            setTimeout(()=>{
                props.history.push({pathname: `/sellproducts`})
            },1500)
            setMsg(res.data);
         } )
        .catch(error => console.log(error));
    }

     const getDetailsClient = () =>{
         
        
     }

     const selectShopAssistants = (id) =>{
        let dataShop = Object.assign({}, data, {
            shop_assistants_id	: id
        });
        setDataForm( dataShop ); 
    }

    const selectShop = (id) =>{
        Axios.get(`/shop_assistants/onlyselected?id=${id}`)
        .then((res) => {
            setShopAssistants(res.data)
        })
        let data1 = Object.assign({}, data, {
            shop_id: id
        });
        setDataForm( data1 );
        setShopId(id);
         console.log(id);
         Axios({
            method: 'GET',
            url: `/products/active?id=${id}`
        })
            .then(res => {
                setProducts(res.data)
                setFilteredProducts(res.data)
            })
            .catch(err => console.log(err))
        
    }
  

    const getShop = () => {
        Axios({
            method: 'GET',
            url: '/shops'
        })
            .then(res => {
                setShops(res.data)
            })
            .catch(err => console.log(err))
    }


    useEffect(() => {
        let clients = props.history.location.state;
        setClientsInfo(clients);
        let data1 = {
        id: clients.id,
        client_id: clients.clientID
        };
        setDataForm( data1 );
        setClientId(clients.productId);
        setSaveProductId(clients.productId);
        getDetailsClient();
        getShop();
    }, [props]);
   

    return(
             <div className="row">
            <div className="col-lg-12 client_wrapper">
                <div className="row">
                    <div className="col-md-6 p-4">
                        <div className="title__box">
                            <h3 className="title">Update Sale for Client</h3>
                        </div>
                        <h4><b>{clientInfo?.name}  {clientInfo?.surname}</b></h4>
                        <p> {clientInfo?.address}  {clientInfo?.phone_number}</p>
                    </div>
                </div>
                <div className="form-group">
                <div className="row">
                    <div className="col-md-6">
                    <p>Select Shop:</p>
                    <ul>
                        {shops?.map((el, index) => {
                            return(
                            <li className={(Number(el.id) === Number(shopId)) ? "active" : `list_style`} key={index} onClick={()=> selectShop(el.id)}>
                                {el.name} - {el.city}
                            </li>
                            )
                        })}
                    </ul>
                    </div>
                    <div className="col-md-6">
                    <p>Select Assistants:</p>
                    <ul>
                        {shopAssistants?.map((el, index) => {
                            return(
                            <li className={(Number(el.id) === Number(data.shop_assistants_id)) ? "active" : `list_style`} key={index} onClick={()=> selectShopAssistants(el.id)}>
                                {el.name}
                            </li>
                            )
                        })}
                    </ul>
                    </div>
                </div>
                </div>
                <div className="row">
                    <div className="col-md-12">
                        <hr />
                        <form>
                            <div className="form-group">
                                <p>Select Product:</p>
                                <div className="search_area mb-5">
                                    <div className="active-pink-4 mb-4 __search_bar">
                                        <input className="form-control" type="text" onChange={searcherProducts} placeholder="Search Product" aria-label="Search"/>
                                    </div>
                                </div>
                                <hr/>
                                <ul>
                                    {filterProducts?.map((el, index) => {
                                        return(
                                        <li className={(Number(el.id) === Number(clientId)) ? "active" : "list_style"} key={index} onClick={()=> selectClient(el.id)}>
                                            {el.description} {el.validity} {el.quantity}
                                        </li>
                                        )
                                    })}
                               </ul>
                           </div>
                            <hr />
                                <button onClick={updateRelation} className="btn btn-outline-dark">Update</button>
                        </form>
                            {msg && <p className="p-3" style={{color: "green"}}><b>Update succesfully!</b></p>}
                        <hr />
                    </div>
                </div> 
            </div>
        </div>
    )
}

export default UpdateSale;