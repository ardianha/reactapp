import React, {useState, useEffect} from 'react';
import './Clients.scss';

// libraries

import Axios from '../../axios';

const Clients = (props) => {
    const [ editMode, setEditMode ] = useState(false);
    const [ editModeButton, setEditModeButton ] = useState(false);
    const [ clients, setClients ] = useState();
    const [ showForm, setShowForm ] = useState(false);
    let [ dataForm, setDataForm ] = useState({});
    let [ msg, setMsg ] = useState("");

    const hideForm = () => {
        setShowForm(!showForm);
        setDataForm({});
        if(showForm){
            setEditModeButton(false);
            setDataForm({});
        }
    } 

    const editButton = () =>{
        setEditMode(!editMode);
        setEditModeButton(false);
        if(showForm){
            setShowForm(false);
        }
    }

    const formName = (e) =>{
        let data = Object.assign({}, dataForm, {
            name: e.target.value
        });
        setDataForm( data );
        // console.log(data) ;
    }
    
    const formSurname = (e) =>{
        let data = Object.assign({}, dataForm, {
            surname: e.target.value
        });
        setDataForm(data);
    }
    
    const formPhonenumber = (e) =>{
        let data = Object.assign({}, dataForm, {
            phone_number: e.target.value
        });
        setDataForm(data);
    }

    const formAddress = (e) =>{
        let data = Object.assign({}, dataForm, {
            address: e.target.value
        });
        setDataForm(data);
    }

    const updateClient = (e) => {
        e.preventDefault();
        Axios.put("/clients/put", dataForm)
        .then(res => {
            getClients();
         } )
        .catch(error => console.log(error));
        setShowForm(false); 
    }

    const addClient = (e) => {
        e.preventDefault();
        Axios.post("/clients/post", dataForm)
        .then(res =>{
            setTimeout(() =>{
                setShowForm(false);
                setDataForm({});
                getClients();
                setMsg("");
            }, 1500);
            setMsg(res.data);
        })
        .catch(error => console.log(error));
    }

    const deleteClient = (id) => {
         Axios.delete(`/clients/delete`, {data : { id: id }} )
        .then(res => {
            getClients();
            if(res.data.name){
            }
        } )
        .catch(error => console.log(error));
    }

    const clientSingle = (el) =>{
        props.history.push({
            pathname: `/clients/single`,
            search: `?id=${el.id}`,
            state: {
                // id: el.id,
                name: el.name,
                surname: el.surname, 
                address: el.address, 
                phone_number: el.phone_number
            }
        });
    }

    const editClient = (el) => {
        let data = Object.assign({}, dataForm, {
            id: el.id,
            name: el.name,
            surname: el.surname,
            address: el.address,
            phone_number: el.phone_number
        });
        setDataForm(data);
        setShowForm(true);
        setEditModeButton(true);
    }

    const getClients = () =>{
        Axios.get('/clients/all')
        .then((res) => setClients(res.data))
        .catch((error) => console.log(error))
    }

    useEffect(() => {
      getClients();
    },[]);

    return (
        <div className="row">
            <div className="col-lg-12 client_wrapper">
                <div className="row">
                    <div className="col-md-6 p-4">
                        <div className="title__box">
                            <h3 className="title">Clients</h3>
                        </div>
                    </div>
                    <div className="col-md-6 p-4">
                       
                        {showForm 
                            ?  
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Hide Form Add</button>
                            : 
                            <button onClick={() => hideForm()} className="btn btn-outline-dark">Register Clients</button>
                        }  
                          {editMode 
                            ?  
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Hide edit mode</button>
                            : 
                            <button onClick={() => editButton()} className="btn btn-outline-dark ml-4">Edit</button>
                        }  
                       
                    </div>
                </div>
                {showForm && 
                <div className="row">
                    <div className="col-md-12">
                        <hr />
                        <div className="title__box p-3">
                            {editModeButton ?
                                <h3 className="title">Edit Client</h3>
                                :
                                <h3 className="title">Add Client</h3>
                            }
                        </div>
                        <form>
                            <div className="form-group">
                            <label>Name:</label>
                                <input type="text" onChange={formName}  value={dataForm.name || ""} className="form-control" placeholder="Name" name="Name" required/>
                            </div>
                            <div className="form-group">
                            <label>Surname:</label>
                                <input type="text"  onChange={formSurname}  value={dataForm.surname || ""} className="form-control"  placeholder="Surname" name="Surname" required/>
                            </div>
                            <div className="form-group">
                            <label>Address:</label>
                                <input type="text"  onChange={formAddress}  value={dataForm.address || ""} className="form-control"  placeholder="Address" name="Address" required/>
                            </div>
                            <div className="form-group">
                            <label>Phone number:</label>
                                <input type="text"  onChange={formPhonenumber}  value={dataForm.phone_number || ""} className="form-control"  placeholder="Phone number" name="phone_number" required/>
                            </div>
                            
                            {editModeButton ?
                                <button onClick={updateClient} className="btn btn-outline-dark">Update Clients</button>
                                :
                                <button onClick={addClient} className="btn btn-outline-dark">Add Clients</button>
                                }
                        </form>
                            {msg && <p className="p-3" style={{ color: "green"}}><b>{msg.msg}</b></p>}
                        <hr />
                    </div>
                </div> }
                <div className="row">
                    <div className="col-lg-12">
                        <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Surname</th>
                                    <th scope="col">Address</th>
                                    <th scope="col">Phone number</th>
                                    {editMode ? <th scope="col">Edit | Delete</th> : <th></th>}
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    clients?.map((el, index) => {
                                        return (
                                            <tr className="client_view"  key={index}>
                                                <td>{el.name}</td>
                                                <td>{el.surname}</td>
                                                <td>{el.address}</td>
                                                <td>{el.phone_number}</td>
                                                {editMode ? <td>
                                                <span className="edit_button" onClick={() => editClient(el)}> Edit </span> 
                                                | 
                                                <span  onClick={() => deleteClient(el.id)} className="delete_button"> Delete </span>
                                                </td> : <td><button className="btn btn-sm btn-outline-dark" onClick={()=> clientSingle(el)}>Details</button></td>}
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Clients;